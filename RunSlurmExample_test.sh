#!/bin/bash
# run the script using
# Run.sh should be executable
# chmod 777 Run.sh
# nohup ./Run.sh > Run.log 2>&1 &

folder=/pasteur/projets/policy01/m6aAkker
folderScript=$folder/MeRIPSeq/src/bash_slurm
#project=CecAm
module load Python/2.7.11
module load bedtools/2.25.0
module load samtools/1.9
module load wigToBigWig
module load HTSeq/0.9.1
module load macs/2.1.0

export PATH=$PATH:~/.local/
export LD_LIBRARY_PATH=~/.local/lib/
module load htslib/1.9
module load  gsl/1.15
module load  gcc/8.2.0
export PATH=$PATH:~/tools/WiggleTools/bin/
#module load R/3.5.0                                                                                                                                                                                                       
module load R/3.4.1
#project=Cecum_all
#project=Liver_all_T13
#project=Cecum_2019
#project=CecAm
#project=Liver_2019_T13
#project=Liver_2018_T13
project=Cecum_allWithoutEc

listfile=$folder/ExpDesign/${project}_List.txt 
expdesign_file=$folder/ExpDesign/${project}_exp_design.txt 
array=$(wc -l $listfile | awk '{print $1}')
array="1-"$array
echo $array

module load Python/2.7.11
module load bedtools/2.25.0
module load samtools/1.9 
module load wigToBigWig
module load HTSeq/0.9.1
module load macs/2.1.0  

export PATH=$PATH:~/.local/
export LD_LIBRARY_PATH=~/.local/lib/
module load htslib/1.9
module load  gsl/1.15
module load  gcc/8.2.0
export PATH=$PATH:~/tools/WiggleTools/bin/
#module load R/3.5.2
module load R/3.4.1

# run Mapping
sbatch --array=${array} $folderScript/RunMapping.sh $listfile

# seq depth for creating Seqdepth/STAR_nbReads.txt
sbatch --array=${array} $folderScript/RunSeqDepth.sh $folder/ExpDesign/${project}_Rename.txt

# count number of reads per windows, and calculate median coverage for POI
sbatch --array=${array} $folderScript/RunWindowCovRPMF.sh $listfile 
sbatch --array=${array} $folderScript/RunWindowCovPOI.sh $listfile 

# create median wig for Searching max coverage
sbatch $folderScript/RunMeanBigWig.sh $project all

# create median coverage wig file for each condition
sbatch $folderScript/RunMeanBigWig.sh $project abx
sbatch $folderScript/RunMeanBigWig.sh $project abx_GF
sbatch $folderScript/RunMeanBigWig.sh $project ex_GF
sbatch $folderScript/RunMeanBigWig.sh $project Am
sbatch $folderScript/RunMeanBigWig.sh $project CONV
sbatch $folderScript/RunMeanBigWig.sh $project Ec
sbatch $folderScript/RunMeanBigWig.sh $project GF
sbatch $folderScript/RunMeanBigWig.sh $project Lp
sbatch $folderScript/RunMeanBigWig.sh $project vanco


 # sbatch $folderScript/RunMeanBigWig.sh $project Am_Liver_2018
 # sbatch $folderScript/RunMeanBigWig.sh $project CONV_Liver_2018
 # sbatch $folderScript/RunMeanBigWig.sh $project Ec_Liver_2018
 # sbatch $folderScript/RunMeanBigWig.sh $project GF_Liver_2018
 # sbatch $folderScript/RunMeanBigWig.sh $project Lp_Liver_2018

 # sbatch $folderScript/RunMeanBigWig.sh $project Liver_2018_Am_T13 
 # sbatch $folderScript/RunMeanBigWig.sh $project Liver_2018_CONV_T13
 # sbatch $folderScript/RunMeanBigWig.sh $project Liver_2018_Ec_T13
 # sbatch $folderScript/RunMeanBigWig.sh $project Liver_2018_GF_T13
 # sbatch $folderScript/RunMeanBigWig.sh $project Liver_2018_Lp_T13

 # sbatch $folderScript/RunMeanBigWig.sh $project Liver_Am_T3 
 # sbatch $folderScript/RunMeanBigWig.sh $project Liver_CONV_T3
 # sbatch $folderScript/RunMeanBigWig.sh $project Liver_Ec_T3
 # sbatch $folderScript/RunMeanBigWig.sh $project Liver_GF_T3
 # sbatch $folderScript/RunMeanBigWig.sh $project Liver_Lp_T3


# Run Peak detection for every dataset
sbatch --array=${array} $folderScript/RunPeakDetection.sh $expdesign_file MACS2
sbatch --array=${array} $folderScript/RunPeakDetection.sh $expdesign_file POI
sbatch --array=${array} $folderScript/RunPeakDetection.sh $expdesign_file RPMF
# attention! FIsher ne marche pas si HTSeq est loade pour des histoires obscures de library
module unload HTSeq
sbatch --array=${array} $folderScript/RunPeakDetection.sh $expdesign_file Fisher

# # Finalize peaks detection : filter by peak occurence and regroup with bedtools
sbatch $folderScript/RunFinalize.sh $project POI
sbatch $folderScript/RunFinalize.sh $project MACS2
sbatch $folderScript/RunFinalize.sh $project RPMF
sbatch $folderScript/RunFinalize.sh $project Fisher
# # for every technique ->  
# # final_bedfile = PATH_PEAKS + exp_design_name + '_' + peak_technique + ‘_Raw.bed'

# # Annotate peaks, search for overlapping genes, create gif and bed file, overlap with ref peaks
sbatch $folderScript/RunAnnotation.sh $project Fisher Raw
sbatch $folderScript/RunAnnotation.sh $project POI Raw
sbatch $folderScript/RunAnnotation.sh $project RPMF Raw
sbatch $folderScript/RunAnnotation.sh $project MACS2 Raw
# # final_bedfile = PATH_PEAKS + exp_design_name + '_' + peak_technique + ‘_Raw.bed’


# Search for Max coverage = summit of the peaks
bed_name=Raw
sbatch $folderScript/RunSearchMax.sh $project MACS2 $bed_name
sbatch $folderScript/RunSearchMax.sh $project RPMF $bed_name
sbatch $folderScript/RunSearchMax.sh $project POI $bed_name
sbatch $folderScript/RunSearchMax.sh $project Fisher $bed_name
#  # final_bedfile = PATH_PEAKS + exp_design_name + '_' + peak_technique + ‘_MaxValues.bed’



# Regroup peaks from different techniques
min_number_technique=3
# bed_name=Raw
# sbatch $folderScript/RunRegroup.sh $project $bed_name $min_number_technique
# sbatch $folderScript/RunAnnotation.sh $project All ${bed_name}_$min_number_technique
bed_name=MaxValues
sbatch $folderScript/RunRegroup.sh $project $bed_name $min_number_technique
sbatch $folderScript/RunAnnotation.sh $project All ${bed_name}_$min_number_technique


# Get MaxMaxValues
bed_name_max=${bed_name}_${min_number_technique}
sbatch $folderScript/RunSearchMax.sh $project All $bed_name_max
sbatch $folderScript/RunAnnotation.sh $project All MaxMaxValues_$min_number_technique                                                                                       


# Run HTSeq for peaks
bed_name=Raw
bed_name_peak=${bed_name}_$min_number_technique
# array=1-64
# sbatch --array=${array} $folderScript/RunHTSeqPeaks.sh $listfile $project $bed_name_peak
bed_name=MaxValues 
bed_name_peak=${bed_name}_$min_number_technique
#sbatch --array=${array} $folderScript/RunHTSeqPeaks.sh $listfile $project $bed_name_peak
bed_name=MaxMaxValues 
bed_name_peak=${bed_name}_$min_number_technique
sbatch --array=${array} $folderScript/RunHTSeqPeaks.sh $listfile $project $bed_name_peak

# Run differential analysis
bed_name=Raw
bed_name_peak=${bed_name}_$min_number_technique
#sbatch $folderScript/RunDiffMeth.sh $project $bed_name_peak
bed_name=MaxValues
bed_name_peak=${bed_name}_$min_number_technique
#sbatch $folderScript/RunDiffMeth.sh $project $bed_name_peak
bed_name=MaxMaxValues
bed_name_peak=${bed_name}_$min_number_technique
sbatch $folderScript/RunDiffMeth.sh $project $bed_name_peak

# # run Guitarplot and motif search
#sbatch --array=1-78 $folderScript/RunGuitarPlot.sh ExpDesign/Cecum_all_GuitarPlots.txt
#sbatch --array=1-78 $folderScript/RunGuitarPlot.sh ExpDesign/Liver_all_GuitarPlots.txt
# sbatch --array=1-31 $folderScript/RunGuitarPlot.sh ExpDesign/Cecum_2019_GuitarPlot.txt 
# sbatch --array=1-108 RunMotif.sh ExpDesign/Motif.txt
# sbatch --array=1-8 RunMotif.sh ExpDesign/Motif2.txt

# sbatch --array=1-3 MeRIPSeq/src/bash_slurm/RunFimo.sh PeakDiffExpression/CecAm_MaxMaxValues/Motif/Fimo.sh
# sbatch --array=1-11 MeRIPSeq/src/bash_slurm/RunFimo.sh PeakDiffExpression/CecAm_MaxMaxValues/Motif/Centrimo.sh

# #echo "Your MeRIPSeq pipeline ended "$(date "+%A %B %d at %H:%M:%S") | mailx -s "MeRIPSeq pipeline" "christophe.becavin@pasteur.fr"


